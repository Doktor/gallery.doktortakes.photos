from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.files.storage import DefaultStorage
from django.db import models
from django.db.models.fields.files import ImageFieldFile
from django.db.models.signals import post_delete, post_save
from django.db.utils import IntegrityError
from django.dispatch import receiver
from django.http import HttpRequest
from django.urls import reverse

from rest_framework.request import Request

from photos.fields import JSONField
from photos.models.photo.thumbnail import THUMBNAIL_DISPLAY, THUMBNAIL_LARGE_SQUARE, THUMBNAIL_MEDIUM
from photos.settings_photos import DEFAULT_PATH

import os
from typing import Optional, Union


def get_filename(photo: 'Photo', filename: str, ext: Optional[str] = None) -> str:
    if ext is None:
        _, ext = os.path.splitext(filename)
        ext = ext.lstrip('.')

    ts = photo.taken.strftime("%Y%m%d_%H%M%S")

    return f"{ts}_{photo.short_md5}.{ext}"


def get_original_path(photo: 'Photo', filename: str) -> str:
    return f"original/{get_filename(photo, filename)}"


class Photo(models.Model):
    # Original image

    original = models.ImageField(
        upload_to=get_original_path,
        verbose_name="Original image",
        help_text="Original image with no modifications")
    md5 = models.CharField(
        max_length=32, editable=False, unique=True, verbose_name="MD5")
    original_filename = models.CharField(max_length=1000, blank=True)

    width = models.PositiveIntegerField(default=0, editable=False)
    height = models.PositiveIntegerField(default=0, editable=False)
    file_size = models.CharField(
        max_length=50, editable=False, blank=True)

    # Metadata

    album = models.ForeignKey(
        'Album', on_delete=models.CASCADE, related_name='photos',
        blank=True, null=True)

    taken = models.DateTimeField(editable=False)
    edited = models.DateTimeField(editable=False)

    exif = JSONField(blank=True, verbose_name="EXIF")

    taxa = models.ManyToManyField('Taxon', through="PhotoTaxon", related_name='photos')

    # Bookkeeping

    uploaded = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Created date')
    updated_date = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self) -> str:
        return self.filename

    @property
    def access_level(self):
        if self.album is None:
            from photos.models.album import Allow
            return Allow.PUBLIC

        return self.album.access_level

    def check_access(self, request: Union[HttpRequest, Request]) -> bool:
        if self.album is None:
            return True

        return self.album.check_access(request)

    @property
    def filename(self) -> str:
        return os.path.basename(self.original.name)

    def get_thumbnail(self, type: str) -> Optional['Thumbnail']:
        thumbnails = self.thumbnails.all()
        return next(filter(lambda t: t.type == type, thumbnails), None)

    def get_large_square_thumbnail(self) -> Optional['Thumbnail']:
        return self.get_thumbnail(THUMBNAIL_LARGE_SQUARE)

    def get_meta_thumbnail(self) -> Optional['Thumbnail']:
        return self.get_thumbnail(THUMBNAIL_MEDIUM) or self.get_thumbnail(THUMBNAIL_DISPLAY)

    def get_absolute_url(self) -> str:
        return reverse('photo', args=[self.album.path, self.md5])

    def get_access_code_url(self) -> str:
        return self.get_absolute_url() + self.album.get_access_code_query()

    def get_exif(self) -> dict:
        from photos.utils.metadata import get_exif
        return get_exif(self)

    def get_original(self) -> File:
        return self.original.file

    @property
    def path(self) -> str:
        return self.album.path if self.album else DEFAULT_PATH

    def resave(self) -> None:
        if not self.pk:
            return

        delete = []

        for field_name in 'original', 'image', 'thumbnail', 'square_thumbnail':
            image: ImageFieldFile = getattr(self, field_name)

            # Mark the old file for deletion
            delete.append(image.name)

            # Resave the files: the new path is automatically determined
            filename = os.path.basename(image.name)
            image.save(filename, File(image), save=False)

        try:
            # Save the new paths
            self.save()
        except:
            pass
        else:
            # Delete the old files
            storage = DefaultStorage()

            for name in delete:
                storage.delete(name)

    def save(self, *args, **kwargs) -> None:
        if self.pk:
            super().save(*args, **kwargs)
            return

        try:
            super().save(*args, **kwargs)
        except IntegrityError as e:
            if str(e) == "UNIQUE constraint failed: photos_photo.md5":
                self.delete()
                raise ValidationError(f"Duplicate file: {self.md5}")

    @property
    def short_md5(self) -> str:
        return self.md5[:8]

    class Meta:
        get_latest_by = 'taken'
        ordering = ('taken', 'uploaded')


@receiver(post_delete, sender=Photo,
          dispatch_uid='receiver_delete_photo_thumbnails')
def receiver_delete_photo_thumbnails(sender, instance: Photo, **kwargs) -> None:
    instance.original.delete(save=False)

    for thumbnail in instance.thumbnails.all():
        thumbnail.delete()
