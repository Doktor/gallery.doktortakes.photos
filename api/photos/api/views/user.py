from datetime import datetime as DateTime
from http import HTTPStatus
from zoneinfo import ZoneInfo

from django.contrib.auth import get_user_model, update_session_auth_hash
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import exceptions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from photos.api.serializers import LogInSerializer

User = get_user_model()


def get_formatted_time(dt: DateTime) -> str:
    return dt.astimezone(ZoneInfo("UTC")).strftime("%Y-%m-%d %H:%M:%S UTC+00:00")


@api_view()
@ensure_csrf_cookie
def get_csrf_token(request: Request) -> Response:
    return Response(None, status=HTTPStatus.OK)


@api_view(["POST"])
def get_api_token(request: Request) -> Response:
    serializer = LogInSerializer(data=request.data, context={'request': request})

    if not serializer.is_valid():
        return Response({"error": serializer.errors['non_field_errors'][0]}, status=HTTPStatus.BAD_REQUEST)

    user = serializer.validated_data['user']
    token, _ = Token.objects.get_or_create(user=user)

    return Response({"message": "Logged in successfully.", "token": token.key}, status=HTTPStatus.OK)


@api_view()
def get_current_user(request: Request) -> Response:
    user = request.user

    if user.is_superuser:
        status = "superuser"
    elif user.is_staff:
        status = "staff"
    elif user.is_authenticated:
        status = "user"
    else:
        return Response({"status": "anonymous"})

    return Response({
        "name": user.username,
        "status": status,
        "account_created": get_formatted_time(user.date_joined),
        "last_sign_in": get_formatted_time(user.last_login) if user.last_login is not None else "never",
    })


@api_view(["POST"])
def change_password(request: Request) -> Response:
    user = request.user

    if user.is_anonymous:
        raise exceptions.NotAuthenticated

    data = request.data

    current = data.get('current', '')
    password1 = data.get('password1', '')
    password2 = data.get('password2', '')

    errors = []

    if not current:
        errors.append("Please enter your current password.")

    if not user.check_password(current):
        errors.append("The current password is incorrect.")

    if not password1 or not password2:
        errors.append("Please enter the new password twice.")

    if password1 != password2:
        errors.append("The new passwords don't match.")

    if errors:
        return Response({"errors": errors}, status=HTTPStatus.BAD_REQUEST)

    user.set_password(password1)
    user.save()
    update_session_auth_hash(request, user)

    return Response(status=HTTPStatus.OK)
