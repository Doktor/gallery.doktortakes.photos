from .album import ManageAlbumList, ManageAlbumDetail, ManageAlbumPhotoList
from .group import GroupList
from .license import ManageLicenseList
from .photo import ManagePhotoDetail, ManageRecentPhotoList
from .photo_taxon import ManagePhotoTaxonList, ManagePhotoTaxonDetail
from .taxon import ManageTaxonList, ImportTaxon
from .thumbnail import ManageThumbnailList
from .user import UserList
