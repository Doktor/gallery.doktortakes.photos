# Issue tags

[View all issues](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues)

## Scope

- [frontend](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=frontend): Vue
- [backend](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=backend): Django

### Subscopes

- [authentication](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=authentication): issues related to the authentication system
- [cms](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=cms): issues related to the custom content management system (CMS)
- [styling](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=styling): anything related to CSS

## Type

- [bug](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=bug): defects with an existing feature, such as wrong or unexpected behavior
- [improvement](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=improvement): improvement to an existing feature
- [new feature](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=new+feature): new features
- [content](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=content): issues related to the actual content of the website, such as text or images
- [refactor](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=refactor): issues purely related to code organization and structure
- [dependencies](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=dependencies): issues related to frontend or backend dependencies, including frameworks, libraries, and packages

## Other

- [requirements incomplete](https://gitlab.com/Doktor/gallery.doktortakes.photos/-/issues?label_name%5B%5D=requirements+incomplete): issues that need more details
